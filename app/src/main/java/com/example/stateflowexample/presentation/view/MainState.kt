package com.example.stateflowexample.presentation.view

data class MainState(
    var r: Int = 255,
    var g: Int = 255,
    var b: Int = 255,
)
