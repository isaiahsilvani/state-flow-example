package com.example.stateflowexample.presentation.view

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    private val _state = MutableStateFlow(MainState())
    val state = _state.asStateFlow()

    fun incrementR() {
        viewModelScope.launch {
            val newValue = _state.value.r + 1
            if (isValidRGBValue(newValue)) {
                _state.value = _state.value.copy(
                    r = newValue + 1
                )
            }
        }
    }

    fun decrementR() {
        viewModelScope.launch {
            val newValue = _state.value.r - 1
            if (isValidRGBValue(newValue)) {
                _state.value = _state.value.copy(
                    r = newValue - 1
                )
            }
        }
    }

    fun incrementG() {
        viewModelScope.launch {
            val newValue = _state.value.g + 1
            if (isValidRGBValue(newValue)) {
                _state.value = _state.value.copy(
                    g = newValue + 1
                )
            }
        }
    }

    fun decrementG() {
        viewModelScope.launch {
            val newValue = _state.value.g - 1
            if (isValidRGBValue(newValue)) {
                _state.value = _state.value.copy(
                    g = newValue - 1
                )
            }
        }
    }

    fun incrementB() {
        viewModelScope.launch {
            val newValue = _state.value.b + 1
            if (isValidRGBValue(newValue)) {
                _state.value = _state.value.copy(
                    b = newValue + 1
                )
            }
        }
    }

    fun decrementB() {
        viewModelScope.launch {
            val newValue = _state.value.b - 1
            if (isValidRGBValue(newValue)) {
                _state.value = _state.value.copy(
                    b = newValue - 1
                )
            }
        }
    }

    private fun isValidRGBValue(value: Int) = value in 1..254
}