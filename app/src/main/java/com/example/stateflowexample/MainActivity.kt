package com.example.stateflowexample

import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.Paint
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.stateflowexample.databinding.ActivityMainBinding
import com.example.stateflowexample.presentation.view.MainViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private val mainViewModel: MainViewModel by viewModels()
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initViews()
        initObservers()
    }

    private fun initViews() {
        with(binding) {
            // ADD BUTTONS
            btnAddR.setOnClickListener {
                Log.e("plus", "sdf")
                mainViewModel.incrementR()
            }
            btnAddG.setOnClickListener {
                mainViewModel.incrementG()

            }
            btnAddB.setOnClickListener {
                mainViewModel.incrementB()

            }
            // MINUS BUTTONS
            btnMinusR.setOnClickListener {
                Log.e("minus", "sdf")
                mainViewModel.decrementR()
            }
            btnMinusG.setOnClickListener {
                mainViewModel.decrementG()
            }
            btnMinusB.setOnClickListener {
                mainViewModel.decrementB()
            }
        }
    }

    private fun initObservers() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                mainViewModel.state.collectLatest { state ->
                    val r = state.r
                    val g = state.g
                    val b = state.b
                    with(binding) {
                        tvR.text = r.toString()
                        tvG.text = g.toString()
                        tvB.text = b.toString()
                        // TODO - Figure out how the heck to turn RGB into color
                        class CustomColorDrawable : Drawable() {
                            private val customPaint: Paint = Paint().apply { setARGB(255, state.r, 0, 0) }

                            override fun draw(canvas: Canvas) {
                                // Get the drawable's bounds
                                val width: Int = bounds.width()
                                val height: Int = bounds.height()
                                val radius: Float = Math.min(width, height).toFloat() / 2f

                                // Draw a red circle in the center
                                canvas.drawCircle((width / 2).toFloat(), (height / 2).toFloat(), radius, customPaint)
                            }

                            override fun setAlpha(alpha: Int): Unit {

                            }

                            override fun setColorFilter(colorFilter: ColorFilter?) {

                            }

                            override fun getOpacity(): Int =
                                // Must be PixelFormat.UNKNOWN, TRANSLUCENT, TRANSPARENT, or OPAQUE
                                PixelFormat.OPAQUE
                        }

                    }
                }
            }
        }
    }
}